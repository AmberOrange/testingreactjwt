package se.experis.jwtReact;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JwtReactApplication {

	public static void main(String[] args) {
		SpringApplication.run(JwtReactApplication.class, args);
	}

}
