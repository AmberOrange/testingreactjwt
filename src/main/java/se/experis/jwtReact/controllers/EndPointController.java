package se.experis.jwtReact.controllers;

import com.auth0.jwt.interfaces.DecodedJWT;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import se.experis.jwtReact.models.CommonResponse;
import se.experis.jwtReact.models.SecretPhrase;
import se.experis.jwtReact.models.UserDetails;
import se.experis.jwtReact.utils.JwtTool;
import se.experis.jwtReact.utils.TempStorage;

import java.util.Date;

@RestController
public class EndPointController {

    @PostMapping("/user")
    public ResponseEntity registerUser(@RequestBody UserDetails userDetails) {
        CommonResponse cr = new CommonResponse();
        cr.data = null;
        HttpStatus resp;

        if(userDetails.getUsername() != null
        && userDetails.getPassword() != null) {
            if(userDetails.getSecretPhrase() == null) {
                userDetails.setSecretPhrase("");
            }

            int i;
            if((i = TempStorage.getInstance().addUser(userDetails)) != -1) {
                cr.data = JwtTool.createToken(i);
                cr.message = "Successfully created user!";
                resp = HttpStatus.CREATED;
            } else {
                cr.message = "Error registering user";
                resp = HttpStatus.BAD_REQUEST;
            }
        } else {
            cr.message = "Registration requires both username and password provided";
            resp = HttpStatus.BAD_REQUEST;
        }
        return new ResponseEntity(cr, resp);
    }

    @GetMapping("/user")
    public ResponseEntity loginUser(@RequestBody UserDetails userDetails) {
        CommonResponse cr = new CommonResponse();
        cr.data = null;
        HttpStatus resp;
        int i;

        if ((i = TempStorage.getInstance().checkLogin(userDetails)) != -1) {
            cr.data = JwtTool.createToken(i);
            cr.message = "Successfully logged in!";
            resp = HttpStatus.OK;
        } else {
            cr.message = "Failed to login";
            resp = HttpStatus.BAD_REQUEST;
        }

        return new ResponseEntity(cr, resp);
    }

    @GetMapping("/secret")
    public ResponseEntity getSecretPhrase(@RequestBody SecretPhrase secretPhrase) {
        CommonResponse cr = new CommonResponse();
        cr.data = null;
        HttpStatus resp;
        DecodedJWT jwt;
        if(secretPhrase.token != null) {
            if ((jwt = JwtTool.decodeToken(secretPhrase.token)) != null) {
                cr.data = TempStorage.getInstance().getUserDetails(Integer.parseInt(jwt.getSubject())).getSecretPhrase();
                cr.message = "Successfully retrieved the secret phrase!";
                resp = HttpStatus.OK;
            } else {
                cr.message = "The JWT is invalid, please login again";
                resp = HttpStatus.BAD_REQUEST;
            }
        } else {
            cr.message = "No token found, please make sure you provide one";
            resp = HttpStatus.BAD_REQUEST;
        }
        return new ResponseEntity(cr, resp);
    }

    @PostMapping("/secret")
    public ResponseEntity setSecretPhrase(@RequestBody SecretPhrase secretPhrase) {
        CommonResponse cr = new CommonResponse();
        cr.data = null;
        HttpStatus resp;
        DecodedJWT jwt;
        if(secretPhrase.token != null
        && secretPhrase.secretPhrase != null) {
            if ((jwt = JwtTool.decodeToken(secretPhrase.token)) != null) {
                TempStorage.getInstance().getUserDetails(Integer.parseInt(jwt.getSubject())).setSecretPhrase(secretPhrase.secretPhrase);
                cr.message = "Successfully changed the secret phrase!";
                resp = HttpStatus.OK;
            } else {
                cr.message = "The JWT is invalid, please login again";
                resp = HttpStatus.BAD_REQUEST;
            }
        } else {
            cr.message = "No token found, please make sure you provide one";
            resp = HttpStatus.BAD_REQUEST;
        }
        return new ResponseEntity(cr, resp);
    }
}
