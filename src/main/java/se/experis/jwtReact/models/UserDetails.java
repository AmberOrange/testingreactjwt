package se.experis.jwtReact.models;

public class UserDetails {
    private String username;
    private String password;
    private String secretPhrase;

    public UserDetails(String username, String password, String secretPhrase) {
        this.username = username;
        this.password = password;
        this.secretPhrase = secretPhrase;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getSecretPhrase() {
        return secretPhrase;
    }

    public void setSecretPhrase(String secretPhrase) {
        this.secretPhrase = secretPhrase;
    }

    public int checkLogin(UserDetails userDetails) {
        if(userDetails.username.equals(this.username)) {
            if(userDetails.password.equals(this.password)) {
                return 1;
            }
            return -1;
        }
        return 0;
    }
}
