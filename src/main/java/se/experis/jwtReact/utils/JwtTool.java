package se.experis.jwtReact.utils;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTCreationException;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;

import java.util.Calendar;
import java.util.Date;

public class JwtTool {
    private static final Algorithm ALGORITHM_HS = Algorithm.HMAC256("secretsauce");
    private static final JWTVerifier VERIFIER = JWT.require(ALGORITHM_HS)
            .withIssuer("auth0")
            .build();

    public static String createToken(int i) {
        String token = null;
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        cal.add(Calendar.MINUTE,1);

        try {
            token = JWT.create()
                    .withIssuer("auth0")
                    .withSubject(Integer.toString(i))
                    .withClaim("name", TempStorage.getInstance().getUserDetails(i).getUsername())
                    .withExpiresAt(cal.getTime())
                    .sign(ALGORITHM_HS);
        } catch (JWTCreationException exception){
            //Invalid Signing configuration / Couldn't convert Claims.
            System.out.println("Invalid Signing configuration / Couldn't convert Claims.");
            System.out.println(exception.getMessage());
        }
        return token;
    }

    public static DecodedJWT decodeToken(String token) {
        DecodedJWT jwt = null;
        try {
            jwt = VERIFIER.verify(token);
        } catch (JWTVerificationException exception){
            //Invalid signature/claims
            System.out.println("Invalid signature/claims");
        }
        return jwt;
    }
}
