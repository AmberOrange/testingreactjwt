package se.experis.jwtReact.utils;

import se.experis.jwtReact.models.UserDetails;

import java.util.ArrayList;
import java.util.List;

public class TempStorage {
    private static TempStorage tempStorageInstance;
    public static TempStorage getInstance() {
        if(tempStorageInstance == null) {
            tempStorageInstance = new TempStorage();
        }
        return tempStorageInstance;
    }

    private List<UserDetails> userList;

    private TempStorage() {
        this.userList = new ArrayList<>();
    }

    public int getUserId(String username) {
        for(int i = 0; i < userList.size(); i++) {
            if(username.equals(userList.get(i).getUsername())) {
                return i;
            }
        }
        return -1;
    }

    public UserDetails getUserDetails(int i) {
        if (i >= 0 && i < userList.size()) {
            return userList.get(i);
        }
        return null;
    }

    public int addUser(UserDetails userDetails) {
        if(getUserId(userDetails.getUsername()) == -1) {
            userList.add(userDetails);
            return getUserId(userDetails.getUsername());
        }
        return -1;
    }

    public int checkLogin(UserDetails userDetails) {
        for(int i = 0; i < userList.size(); i++) {
            switch (userDetails.checkLogin(userList.get(i))) {
                case 0:
                    continue;
                case 1:
                    return i;
                case -1:
                    return -1;
            }
        }
        return -1;
    }
}
